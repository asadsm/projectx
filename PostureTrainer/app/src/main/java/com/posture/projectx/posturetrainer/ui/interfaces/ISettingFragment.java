package com.posture.projectx.posturetrainer.ui.interfaces;

/**
 * Created by Asad on 10/31/2016.
 */

public interface ISettingFragment {

    void setVibrate(boolean on);
    void setSoundAlert(boolean on);
    boolean isVibrateOn();
    boolean isSoundAlertOn();

}
