package com.posture.projectx.posturetrainer.ui.utils;

import android.graphics.Color;

import java.util.HashMap;

/**
 * Created by Asad on 11/14/2016.
 */
public class Constants {


    public enum Labels {
        MAGNITUDE(0), X_ACCELERATION(1), Y_ACCELERATION(2), Z_ACCELERATION(3), FLEXSENSOR(4);
        private final int value;

        private Labels(int value){
            this.value = value;
        }

        public int getValue(){
            return value;
        }

    }


    public static HashMap<Labels, Integer> colorMapping = new HashMap<Labels, Integer>();
    static {
        colorMapping.put(Labels.MAGNITUDE, Color.RED);
        colorMapping.put(Labels.FLEXSENSOR, Color.BLUE);
        colorMapping.put(Labels.X_ACCELERATION, Color.GREEN);
        colorMapping.put(Labels.Y_ACCELERATION, Color.YELLOW);
        colorMapping.put(Labels.Z_ACCELERATION, Color.BLACK);
    }

}

