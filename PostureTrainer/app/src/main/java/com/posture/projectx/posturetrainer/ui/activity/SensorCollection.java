package com.posture.projectx.posturetrainer.ui.activity;

/**
 * Created by miaoqi on 11/22/16.
 */

public class SensorCollection {
    long timeStamp;
    double accelerometerX;
    double accelerometerY;
    double accelerometerZ;
    int flexSensor;
    double  magnitude;

    public SensorCollection(
            long timeStamp,
            double accelerometerX,
            double accelerometerY,
            double accelerometerZ,
            int flexSensor,
            double magnitude){
        this.timeStamp = timeStamp;
        this.accelerometerX = accelerometerX;
        this.accelerometerY = accelerometerY;
        this.accelerometerZ = accelerometerZ;
        this.flexSensor = flexSensor;
        this.magnitude = magnitude;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(timeStamp);
        sb.append(",");
        sb.append(accelerometerX);
        sb.append(",");
        sb.append(accelerometerY);
        sb.append(",");
        sb.append(accelerometerZ);
        sb.append(",");
        sb.append(flexSensor);
        sb.append(",");
        sb.append(magnitude);
        sb.append("\n");

        return sb.toString();
    }
}
