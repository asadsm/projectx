package com.posture.projectx.posturetrainer.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.posture.projectx.posturetrainer.R;
import com.posture.projectx.posturetrainer.ui.activity.BluetoothLeService;
import com.posture.projectx.posturetrainer.ui.interfaces.IHomeFragment;
import com.posture.projectx.posturetrainer.R;
import com.posture.projectx.posturetrainer.ui.activity.MainActivity;
import com.posture.projectx.posturetrainer.ui.interfaces.IHomeFragment;
import android.widget.ListView;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;

import org.w3c.dom.Text;

import java.io.FileOutputStream;

import java.io.IOException;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IHomeFragment} interface
 * to handle interaction events.
 */
public class HomeFragment extends Fragment {

    private ViewGroup mRootView;

    Button       connectButton = null;
    Button       calibrateButton = null;
    TextView     accelerometerTextView;
    TextView     ascendRateTextView;
    ImageView    microsoft_connection_icon;
    ImageView    arduino_connection_icon;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // For debug
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    Button btnOn, btnOff, btnDis, startButton, readButton;
    TextView accelerationText;
    SeekBar brightness;
    String address = null;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private String mParam1;
    private String mParam2;

    private IHomeFragment mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);

        connectButton = (Button) mRootView.findViewById(R.id.btnStart);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Home Fragment connected");
                mListener.connect();
            }
        });

        calibrateButton = (Button) mRootView.findViewById(R.id.calibrationStart);
        calibrateButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Flex sensor calibration");
                ((MainActivity) getActivity()).calibration();
            }
        });

        accelerometerTextView        = (TextView)  mRootView.findViewById(R.id.txtStatus);
        ascendRateTextView           = (TextView)  mRootView.findViewById(R.id.ascendTextView);
        microsoft_connection_icon    = (ImageView) mRootView.findViewById(R.id.microsoft_band_connection_icon);
        arduino_connection_icon      = (ImageView) mRootView.findViewById(R.id.arduino_connection_icon);

        if (mListener.isBandConnected())
            microsoft_connection_icon.setImageResource(R.drawable.ic_green_icon);
        else
            microsoft_connection_icon.setImageResource(R.drawable.ic_red_icon);

        if (mListener.isArduinoConnected())
            arduino_connection_icon.setImageResource(R.drawable.ic_green_icon);
        else
            arduino_connection_icon.setImageResource(R.drawable.ic_red_icon);

        return mRootView;
    }


    public void setMicrosoftConnectionStatus(final boolean connected) {
        if (microsoft_connection_icon == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (connected)

                        microsoft_connection_icon.setImageResource(R.drawable.ic_green_icon);
                    else
                        microsoft_connection_icon.setImageResource(R.drawable.ic_red_icon);

                } catch (Exception e){
                    Log.e("ProjectX - ERROR", e.getMessage());
                }
            }
        });
    }

    public void setArduinoConnectionStatus(final boolean connected){
        if (arduino_connection_icon == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (connected)
                        arduino_connection_icon.setImageResource(R.drawable.ic_green_icon);
                    else
                        arduino_connection_icon.setImageResource(R.drawable.ic_red_icon);

                } catch (Exception e){
                    Log.e("ProjectX - ERROR", e.getMessage());
                }
            }
        });


    }
    public void setAccelerometerTextView(String text){
        if (text == null)
            accelerometerTextView.setText("N/A");
        else
            accelerometerTextView.setText(text);
    }

    public void setAscendRateTextView(String text) {
        if (text == null)
            ascendRateTextView.setText("N/A");
        else
            ascendRateTextView.setText(text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IHomeFragment) {
            mListener = (IHomeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
