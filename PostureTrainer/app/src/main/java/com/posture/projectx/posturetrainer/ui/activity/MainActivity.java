package com.posture.projectx.posturetrainer.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.FloatRange;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandIOException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.BandPendingResult;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.notifications.VibrationType;
import com.microsoft.band.sensors.BandAccelerometerEvent;
import com.microsoft.band.sensors.BandAccelerometerEventListener;
import com.microsoft.band.sensors.BandAltimeterEvent;
import com.microsoft.band.sensors.BandAltimeterEventListener;
import com.microsoft.band.sensors.SampleRate;
import com.posture.projectx.posturetrainer.R;
import com.posture.projectx.posturetrainer.ui.fragment.HomeFragment;
import com.posture.projectx.posturetrainer.ui.fragment.SettingFragment;
import com.posture.projectx.posturetrainer.ui.fragment.StatsFragment;
import com.posture.projectx.posturetrainer.ui.interfaces.IHomeFragment;
import com.posture.projectx.posturetrainer.ui.interfaces.ISettingFragment;
import com.posture.projectx.posturetrainer.ui.interfaces.IStatsFragment;
import com.posture.projectx.posturetrainer.ui.utils.Constants;

import java.nio.Buffer;
import java.util.ArrayList;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import java.io.InputStream;
import java.net.URL;

//import opencsv.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Locale;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class MainActivity extends AppCompatActivity implements IStatsFragment, IHomeFragment,
        ISettingFragment {


    // UI/UX Components
    HomeFragment homeFragment       = null;
    StatsFragment statsFragment     = null;
    SettingFragment settingFragment = null;
    Button homeButton               = null;
    Button historyButton            = null;
    Button settingButton            = null;
    TextView txtArduinoStatus       = null;
    MediaPlayer alertPlayer         = null;

    // Variables
    boolean vibrateON               = true;
    boolean soundAlert              = true;
    ArrayList<Float> ascendData     = null;
    double maxRepMag                = -1;
    boolean bandConnected           = false;
    boolean arduinoConnected        = false;

    // One Rep detection
    boolean peakDetected                = false;
    int     peakIndex                   = -1;
    int     numberReps                  = 0;
    ArrayList<SensorCollection> oneRepData;


    // For StatsFragment Graph
    float currentXAcceleration, currentYAcceleration, currentZAcceleration, currentAltimeterRate;

    BandClient bandClient           = null;
    BluetoothAdapter myBluetooth    = null;

    // Preferences (persistent)
    public static final String PREF_FILE      = "SETTINGS";
    public static final String VIBRATE_ON     = "VIBRATE_ON";
    public static final String SOUND_ALERT_ON = "SOUND_ALERT_ON";

    // ////////////////////////////////////////////////////////////////////////////////////
    // LPF setting
    private LPF lpfX, lpfZ;
    public static int ONE_REP_FREQUENCY = 62;
    public static double ONE_REP_PERIOD = 4.5; // in second
    public static double THERSHOLD_X = 1.0;
    public static double THERSHOLD_Z = 1.0;
    public static int MAX_HISTROY_SIZE = 512;
    public static double DEGRAGATION_FACTOR = 0.3;
    public static double NOISE_TOLERANCE = 0.3;
    public static int PEAK_INDEX_OFF = 4;
    public static int WINDOW_LENGTH = 10;
    public static int AVERAGE_X = 0;
    public static int AVERAGE_Y = 0;
    public static int AVERAGE_Z = 0;
    // End of LPF setting

    // One rep detection setting
    public OneRepProcess repProcess;

    // Sensor data histroy and upload
    public LinkedList<SensorCollection> unUploadedHistroy;
    int cachedFlexSensor;
    int cachedRawFlexSensor;
    int calibrationOffset;
    double cachedAltimeter;
    int repsCounter;
    int intervalCounter;
    boolean keepRecord;

    int repCounter = 0;
    LinkedList<String> resultList;

    // End of One rep detection setting
    // ////////////////////////////////////////////////////////////////////////////////////

    // ////////////////////////////////////////////////////////////////////////////////////
    // Arduino setting
    // View

    // Service member
    private Intent gattServiceIntent;
    private BluetoothLeService mBluetoothLeService;
    private boolean serviceStart = false;

    // Constants
    private String mDeviceAddress = "98:4F:EE:0F:A2:18";
    public final static String ARDUINO_FLEXSENSOR_SERVICE = "00001801-e8f2-537e-4f6c-d104768a1214";
    public final static String ARDUINO_FLEXSENSOR_MEASUREMENT = "00002a05-e8f2-537e-4f6c-d104768a1214";

    public final static UUID UUID_ARDUINO_FLEXSENSOR_SERVICE  =
            UUID.fromString(ARDUINO_FLEXSENSOR_SERVICE);
    public final static UUID UUID_ARDUINO_FLEXSENSOR_MEASUREMENT =
            UUID.fromString(ARDUINO_FLEXSENSOR_MEASUREMENT);

    // For debug
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    // End of Arduino setting
    // ////////////////////////////////////////////////////////////////////////////////////


    public BandAltimeterEventListener mBandAltimeterEventListener = new BandAltimeterEventListener() {

        @Override
        public void onBandAltimeterChanged(BandAltimeterEvent event) {
            if (event != null) {
                try {
                    currentAltimeterRate = event.getRate();
                    final String text = "Rate of ascend/descend\n" + String.format(Locale.ENGLISH,"\nRate = %.3f \n",
                            currentAltimeterRate);

                    cachedAltimeter = currentAltimeterRate;
                    ascendData.add(currentAltimeterRate);

                    if (ascendData.size() == 5)
                        ascendData.remove(0);


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            homeFragment.setAscendRateTextView(text);
                        }
                    });
                    //homeFragment.setAccelerometerTextView(text);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("ProjectX - ERROR","Could not find instance of HomeFragment");
                }

            }
        }

    };

    public BandAccelerometerEventListener mAccelerometerEventListener = new BandAccelerometerEventListener() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onBandAccelerometerChanged(final BandAccelerometerEvent event) {
            /*
            When band moves up
            Z becomes positive (need to use low pass filter to get rid of gravity
            X becomes negative if buttons angled towards forearm (away from hand)
            X becomes positive if buttons angled towards hand
            We should be able to ignore the Y axis as the movement shouldn't be in this direction for the exercise
            */
            if (event != null) {

                currentXAcceleration = event.getAccelerationX();
                currentYAcceleration = event.getAccelerationY();
                currentZAcceleration = event.getAccelerationZ();

                final double magnitude = Math.abs(
                        Math.sqrt(
                                Math.pow(currentXAcceleration, 2)
                                        + Math.pow(currentZAcceleration,2))
                );

                SensorCollection newRecord = new SensorCollection(
                        System.currentTimeMillis(),
                        currentXAcceleration,
                        currentYAcceleration,
                        currentZAcceleration,
                        cachedFlexSensor,
                        magnitude);


                oneRepData.add(newRecord);
                if (oneRepData.size() == 37){
                    oneRepData.remove(0);
                }

                double firstAvg = 0, midAvg = 0, lastAvg = 0, maxZ=0;
                float maxPeak = -1;
                // First 1.5 second avg
                int i  = 0;
                if (oneRepData.size() == 36){
                    for (i = 0; i < 12; i++){
                        firstAvg += oneRepData.get(i).magnitude;
                    }
                    firstAvg /= 12;
                    for (; i < 24; i++){
                        midAvg += oneRepData.get(i).magnitude;
                        if (oneRepData.get(i).magnitude > maxPeak) {
                            maxPeak = (float) oneRepData.get(i).magnitude;
                            maxZ = oneRepData.get(i).accelerometerZ;
                        }
                    }
                    midAvg /= 12;
                    for (; i < 36; i++){
                        lastAvg += oneRepData.get(i).magnitude;
                    }
                    lastAvg /= 12;

                }

                // Detect peak
                if (! peakDetected) {
                    if (maxPeak >  -1)
                        peakDetected = 5 * maxPeak > 6 && 5 * maxZ > 5;
                }
                // If peak found
                if (peakDetected){


                    if (Math.abs((double) (firstAvg - lastAvg)) < 0.1){

                        numberReps++;
                        uploadData(oneRepData);
                        peakDetected = false;

                        // Send one rep data to server
                        oneRepData.clear();

                        peakIndex = -1;
                        final boolean hasRep = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Context context = getApplicationContext();
                                CharSequence toastText = "One Rep!!";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, toastText, duration);
                                toast.show();


/*                         if (bandClient != null){
                            try {
                                if (vibrateON) {
                                    bandClient.getNotificationManager().vibrate(VibrationType.NOTIFICATION_TWO_TONE);
                                    ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(1000);
                                }
                                if (soundAlert)
                                    alertPlayer.start();


                            }
                            catch (BandIOException e) {
                                Log.d("ProjectX - ERROR", "Could not send vibration to band");
                            }
                        }*/





                            }
                        });

                    }
                }



                if (magnitude > maxRepMag)
                    maxRepMag = magnitude;


                try {
                    final String text = "Acceleration\n" + String.format(Locale.ENGLISH, " X = %.3f \n Y = %.3f\n Z = %.3f\n M = %.3f\n", currentXAcceleration,
                            currentYAcceleration, currentZAcceleration, magnitude);

                    boolean oneRep;

                    ArrayList<Constants.Labels> labelList = new ArrayList<>();
                    labelList.add(Constants.Labels.MAGNITUDE);
                    labelList.add(Constants.Labels.X_ACCELERATION);
                    labelList.add(Constants.Labels.Y_ACCELERATION);
                    labelList.add(Constants.Labels.Z_ACCELERATION);
                    //labelList.add(Constants.Labels.FLEXSENSOR);

                    ArrayList<Float> currentValues = new ArrayList<>();
                    currentValues.add((float) magnitude);
                    currentValues.add(currentXAcceleration);
                    currentValues.add(currentYAcceleration);
                    currentValues.add(currentZAcceleration);
                    //currentValues.add((float)cachedFlexSensor * 5 / 1024);
                    statsFragment.addDataToGraph(labelList,currentValues);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("ProjectX - ERROR","Could not find instance of HomeFragment");
                }

                /*if (magnitude > 2){
                    if (bandClient != null){
                        try {
                            if (vibrateON) {
                                bandClient.getNotificationManager().vibrate(VibrationType.NOTIFICATION_TWO_TONE);
                                ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(1000);
                            }
                            if (soundAlert)
                                alertPlayer.start();


                        }
                        catch (BandIOException e) {
                            Log.d("ProjectX - ERROR", "Could not send vibration to band");
                        }
                    }



                }*/


            }
        }
    };

                @Override
                protected void onCreate (Bundle savedInstanceState){

                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.activity_main);

                    // Restore preferences
                    SharedPreferences settings = getSharedPreferences(PREF_FILE, 0);
                    vibrateON = settings.getBoolean(VIBRATE_ON, true);
                    soundAlert = settings.getBoolean(SOUND_ALERT_ON, true);
                    ascendData = new ArrayList<Float>();
                    // Set buttons
                    homeButton = (Button) findViewById(R.id.button_home);
                    historyButton = (Button) findViewById(R.id.button_history);
                    settingButton = (Button) findViewById(R.id.button_settings);

                    homeFragment = new HomeFragment();
                    settingFragment = new SettingFragment();
                    statsFragment = new StatsFragment();

                    resultList = new LinkedList<String>();
                    resultList.add("Good Reps");
                    resultList.add("Good Reps");
                    resultList.add("Good Reps");
                    resultList.add("Good Reps");
                    resultList.add("Bad Reps");
                    resultList.add("Bad Reps");
                    resultList.add("Good Reps");
                    resultList.add("Bad Reps");

                    alertPlayer = MediaPlayer.create(this, R.raw.doh_sound_alert);
                    // LPF Setup
                    lpfX = new LPF();
                    lpfZ = new LPF();
                    repProcess = new OneRepProcess();
                    unUploadedHistroy = new LinkedList<SensorCollection>();
                    cachedFlexSensor = 0;
                    cachedRawFlexSensor = 0;
                    calibrationOffset = 0;
                    cachedAltimeter = 0;
                    repsCounter = 0;
                    intervalCounter = 0;
                    keepRecord = false;
                    // Arduino Setup
                    txtArduinoStatus = (TextView) findViewById(R.id.txtArduinoStatus);
                    gattServiceIntent = new Intent(this, BluetoothLeService.class);
                    Log.d(TAG, "Bind Service.");
                    txtArduinoStatus.setText("Start Connecting to: " + mDeviceAddress);
                    bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
                    if (!serviceStart) {
                        startService(gattServiceIntent);
                        serviceStart = true;
                    }

                    myBluetooth = BluetoothAdapter.getDefaultAdapter();

                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, homeFragment).commit();

                    homeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, homeFragment).commit();
                        }
                    });

                    historyButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, statsFragment).commit();
                        }
                    });

                    settingButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, settingFragment).commit();
                        }
                    });

                }

                @Override
                protected void onResume () {
                    super.onResume();
                    oneRepData           = new ArrayList<>();

                    ////////////// Arduino BLE //////////////
                    registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
                    if (mBluetoothLeService != null) {
                        final boolean result = mBluetoothLeService.connect(mDeviceAddress);
                        Log.d(TAG, "Connect request result=" + result);
                    }
                    /////////////////////////////////////////
                }

                @Override
                protected void onPause () {
                    super.onPause();
                    if (bandClient != null) {
                        try {
                            bandClient.getSensorManager().unregisterAllListeners();
                        } catch (BandIOException e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                    ////////////// Arduino BLE //////////////
                    unregisterReceiver(mGattUpdateReceiver);
                    /////////////////////////////////////////
                }

                @Override
                protected void onDestroy () {
                    if (bandClient != null) {
                        try {
                            bandClient.disconnect().await();
                        }
                        catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                    }
                    ////////////// Arduino BLE //////////////
                    unbindService(mServiceConnection);
                    stopService(gattServiceIntent);
                    mBluetoothLeService = null;
                    /////////////////////////////////////////
                    super.onDestroy();
                }

            private boolean getConnectedBandClient ()throws InterruptedException, BandException {
                if (bandClient == null) {
                    // Find paired bands
                    BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
                    if (devices.length == 0) {
                        return false;
                    }
                    bandClient = BandClientManager.getInstance().create(getBaseContext(), devices[0]);
                } else if (ConnectionState.CONNECTED == bandClient.getConnectionState()) {
                    return true;
                }

                BandPendingResult<ConnectionState> pendingResult = bandClient.connect();
                ConnectionState state = pendingResult.await();
                return ConnectionState.CONNECTED == state;
            }

            @Override
            public void connect () {

                new AccelerometerSubscriptionTask().execute();
            }

            private AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView av, View v, int arg2, long arg3) {

                    // Get the device MAC address, the last 17 chars in the View
                    String info = ((TextView) v).getText().toString();
                    String address = info.substring(info.length() - 17);
                    // Make an intent to start next activity.
                    Intent i = new Intent(MainActivity.this, HomeFragment.class);
                    //Change the activity.

                    // MAY NEED BELOW ?????
                    //   i.putExtra(EXTRA_ADDRESS, address); //this will be received at ledControl (class) Activity
                    startActivity(i);
                }
            };


            @Override
            public void setSoundAlert ( boolean on){
                soundAlert = on;
                SharedPreferences sharedPref = getSharedPreferences(PREF_FILE, 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(SOUND_ALERT_ON, on);
                editor.apply();
            }

            @Override
            public boolean isVibrateOn () {
                return vibrateON;
            }

            @Override
            public boolean isSoundAlertOn () {
                return soundAlert;
            }

            @Override
            public void setVibrate ( boolean on){
                vibrateON = on;
                SharedPreferences sharedPref = getSharedPreferences(PREF_FILE, 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(VIBRATE_ON, on);
                editor.apply();
            }

            @Override
            public void addEntryToGraph (LineChart mChart){


            }

            private class AccelerometerSubscriptionTask extends AsyncTask<Void, Void, Void> {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        if (getConnectedBandClient()) {
                            Log.d(TAG, "Band is connected");
                            bandConnected = true;
                            bandClient.getSensorManager().registerAccelerometerEventListener(mAccelerometerEventListener, SampleRate.MS128);
                            bandClient.getSensorManager().registerAltimeterEventListener(mBandAltimeterEventListener);
                        } else {
                            bandConnected = false;
                            Log.d(TAG, "Band isn't connected. Please make sure bluetooth is on and the band is in range");
                            homeFragment.setMicrosoftConnectionStatus(false);

                        }
                    } catch (BandException e) {
                        bandConnected =  false;
                        String exceptionMessage = "";
                        homeFragment.setMicrosoftConnectionStatus(false);

                        switch (e.getErrorType()) {
                            case UNSUPPORTED_SDK_VERSION_ERROR:
                                exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                                break;
                            case SERVICE_ERROR:
                                exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                                break;
                            default:
                                exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                                break;
                        }
                        Log.e(TAG, exceptionMessage);

                    } catch (Exception e) {
                        bandConnected = false;
                        Log.e(TAG, e.getMessage());
                    }

                    homeFragment.setMicrosoftConnectionStatus(bandConnected);

                    return null;
                }
            }


            // ////////////////////////////////////////////////////////////////////////////////////
            // LPF setup
            static double filter_taps[] = {
                    -4.4408920985006264e-17,
                    4.4408920985006264e-17,
                    1,
                    4.4408920985006264e-17,
                    -4.4408920985006264e-17
            };
            public static int SAMPLEFILTER_TAP_NUM = 5;
            public class LPF {
                private double[] history = new double[SAMPLEFILTER_TAP_NUM];
                private int last_index = 0;

                void filterPut(double input) {
                    history[last_index] = input;
                    last_index = (last_index + 1) % SAMPLEFILTER_TAP_NUM;
                }

                double filterGet() {
                    double acc = 0;
                    int index = last_index;
                    for (int i = 0; i < SAMPLEFILTER_TAP_NUM; ++i) {
                        index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
                        acc += history[index] * filter_taps[i];
                    }
                    ;
                    return acc;
                }
            }
            // End of LPF setup
            // ////////////////////////////////////////////////////////////////////////////////////

            // ////////////////////////////////////////////////////////////////////////////////////
            // Histroy process setup
            public class OneRepProcess {
                private Histroy histroyX;
                private Histroy histroyZ;
                private boolean oneRepStatus;
                private boolean startListening;
                private int listeningCycle;
                private int peakAverage;
                private int resumeAverage;
                private int startListeningZIndex;
                private int accumulateVarianceX;

                OneRepProcess() {
                    histroyX = new Histroy();
                    histroyZ = new Histroy();
                    oneRepStatus = false;
                    startListening = false;
                    listeningCycle = 0;
                    peakAverage = 0;
                    resumeAverage = 0;
                    startListeningZIndex = 0;
                    accumulateVarianceX = 0;
                }

                void putX(double newX) {
                    histroyX.put(newX);

                }

                void putZ(double newZ) {
                    histroyZ.put(newZ);
                }

                boolean getOneRepStatus() {
                    updateOneRepStatus();
                    return oneRepStatus;
                }

                boolean getStartListening() {
                    return startListening;
                }
                void clearStatus() {
                    histroyX.clearStatus();
                    histroyZ.clearStatus();
                }

                // TODO: return the arrays of date within the latest one rep interval
                // need to define how to set the window
                public double[] getOneRepHistroyX() {
                    return new double[1];
                    //return new Arrays.copyOfRange(histroyX.getHistroy(), histroyX.getBottomIndex(), histroyX.getPeakIndex());
                }

                void resetListening(){
                    startListening = false;
                    listeningCycle = 0;
                    peakAverage = 0;
                    resumeAverage = 0;
                    accumulateVarianceX = 0;
                }
                // TODO: finish one rep detection given the peak/bottom status
                private void updateOneRepStatus() {
                    // Start a new analysis windows
                    if(histroyZ.getCurrentValue() - AVERAGE_Z > THERSHOLD_Z){
                        startListeningZIndex = histroyZ.getCurrentIndex();
                        resetListening();
                        startListening = true;
                    }
                    if(startListening){
                        if(listeningCycle < WINDOW_LENGTH){
                            peakAverage += histroyZ.getCurrentValue();
                        } else if(listeningCycle == WINDOW_LENGTH) {
                            peakAverage /= WINDOW_LENGTH;
                            if (peakAverage < THERSHOLD_Z * DEGRAGATION_FACTOR) {
                                resetListening();
                                accumulateVarianceX += (histroyX.getCurrentValue() - AVERAGE_X) * (histroyX.getCurrentValue() - AVERAGE_X);
                            }
                        } else if(listeningCycle > WINDOW_LENGTH && listeningCycle < WINDOW_LENGTH * 2){
                            // wait for a while
                        } else if(listeningCycle >= WINDOW_LENGTH * 2 && listeningCycle < WINDOW_LENGTH * 3){
                            resumeAverage += histroyZ.getCurrentValue();
                        } else if(listeningCycle == WINDOW_LENGTH * 3){
                            resumeAverage /= WINDOW_LENGTH;
                            if(Math.abs(resumeAverage - AVERAGE_Z) < NOISE_TOLERANCE) {
                                if(accumulateVarianceX > THERSHOLD_X ){
                                    oneRepStatus = true;
                                }
                            }
                            resetListening();
                        } else {
                            resetListening();
                        }

                    } else {
                        resetListening();
                    }

                    //oneRepStatus = (histroyX.maxDelta() > THERSHOLD_X && histroyZ.maxDelta() > THERSHOLD_Z);

                    //Log.i(TAG, "Delta X:" + histroyX.maxDelta());
                    //Log.i(TAG, "Delta Z:" + histroyZ.maxDelta());
                }

                // store filtered data to detect one rep
                private class Histroy {
                    private int peakIndex = 0, bottomIndex = 0;
                    private int currentIndex = 0;
                    private double[] histroy;

                    Histroy() {
                        histroy = new double[(int) (ONE_REP_FREQUENCY * ONE_REP_PERIOD)];
                    }

                    void put(double newValue) {
                        updateStatus(newValue);
                        histroy[currentIndex] = newValue;
                        currentIndex = (currentIndex + 1) % histroy.length;
                    }

                    // Need to deal with protential noisy
                    // local peak/bottom should be different from the global peak/bottom
                    private void updateStatus(double newValue) {
                        if (newValue > histroy[peakIndex]) peakIndex = currentIndex;
                        if (newValue < histroy[bottomIndex]) bottomIndex = currentIndex;
                    }

                    public int getPeakIndex() {
                        return peakIndex;
                    }

                    public int getBottomIndex() {
                        return bottomIndex;
                    }

                    public double getCurrentValue() {
                        return histroy[currentIndex];
                    }

                    public double[] getHistroy() {
                        return histroy;
                    }

                    private void clearStatus() {
                        peakIndex = 0;
                        bottomIndex = 0;
                        currentIndex = 0;
                        histroy = new double[(int)(ONE_REP_FREQUENCY * ONE_REP_PERIOD)];
                    }

                    private double maxDelta() {
                        return histroy[peakIndex] - histroy[bottomIndex];
                    }

                    public double getPead(){
                        return histroy[peakIndex];
                    }

                    public double getBottom(){
                        return histroy[bottomIndex];
                    }

                    public int getCurrentIndex(){
                        return currentIndex;
                    }
                }

            }

            public void uploadData(ArrayList<SensorCollection> array){
                Log.i(TAG, "Write to file in detect");
                File file = getFileStreamPath("myFile.csv");
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to create new file");
                    }
                }
                try {
                    FileOutputStream fileOutputStream = openFileOutput(file.getName(), Context.MODE_PRIVATE);
                    while(array.size() > 0){
                        fileOutputStream.write(array.remove(0).toString().getBytes());
                    }
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error in writing sensor data into local file");
                }
                Log.i(TAG, "Upload to server in detect");
                // upload to server
                HttpConnect();
            }

            // End of Histroy process setup
            // ////////////////////////////////////////////////////////////////////////////////////

            // ////////////////////////////////////////////////////////////////////////////////////
            // Arduino setup

            private final ServiceConnection mServiceConnection = new ServiceConnection() {

                @Override
                public void onServiceConnected(ComponentName componentName, IBinder service) {
                    Log.i(TAG, "onServiceConnected");
                    mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
                    if (!mBluetoothLeService.initialize()) {
                        displayData("");
                        Log.e(TAG, "Unable to initialize Bluetooth");
                        finish();
                    }
                    // Automatically connects to the device upon successful start-up initialization.
                    Log.i(TAG, "Try to connect to " + mDeviceAddress);
                    mBluetoothLeService.connect(mDeviceAddress);
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    mBluetoothLeService = null;
                }
            };

            // Handles various events fired by the Service.
            // ACTION_GATT_CONNECTED: connected to a GATT server.
            // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
            // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
            // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
            //                        or notification operations.
            private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final String action = intent.getAction();
                    if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                        Log.i(TAG, "Service Conncected");
                        displayData("Service Connected");
                        invalidateOptionsMenu();
                    } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                        Log.i(TAG, "Service Disconnected");
                        displayData("Service Disconnected");
                        invalidateOptionsMenu();
                    } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                        // Show all the supported services and characteristics on the user interface.
                        Log.i(TAG, "Find Service");
                        displayData("Find Service");
                        connectToFlexServices(mBluetoothLeService.getSupportedGattServices());
                    } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                        // TODO: store the flex sensor into another buffer for later processing
                        // NEW FLEX SENSOR DATA IS AVAILABLE HERE!!!
                        // Log.i(TAG, "ACTION DATA AVAILABLE");
                        int newFlexSensor = Integer.valueOf(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                        cachedRawFlexSensor = newFlexSensor;
                        cachedFlexSensor = cachedRawFlexSensor - calibrationOffset;
                        displayData("Flex Sensor raw value: " + cachedRawFlexSensor + ". Calibrated value: " + cachedFlexSensor);
                    }
                }
            };


        private void displayData(String data) {
            if (data != null) {
                txtArduinoStatus.setText(data);
            }
        }

        private static IntentFilter makeGattUpdateIntentFilter() {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
            intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
            intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
            intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
            return intentFilter;
        }

        private void connectToFlexServices(List<BluetoothGattService> gattServices) {
            if (gattServices == null) return;
            boolean flexSensorServiceFound = false;
            boolean flexSensorCharFound = false;
            BluetoothGattCharacteristic gattChar = new BluetoothGattCharacteristic(UUID_ARDUINO_FLEXSENSOR_MEASUREMENT, 0, 0);
            BluetoothGattService gattServiceDisplay = new BluetoothGattService(UUID_ARDUINO_FLEXSENSOR_SERVICE, 0);
            // Loops through available GATT Services.
            for (BluetoothGattService gattService : gattServices) {
                //Log.d(TAG, "Service uuid: " + gattService.getUuid().toString());
                //Log.d(TAG, "Service constant uuid: " + UUID_ARDUINO_FLEXSENSOR_SERVICE.toString());
                if (UUID_ARDUINO_FLEXSENSOR_SERVICE.toString().equals(gattService.getUuid().toString())) {
                    gattServiceDisplay = gattService;
                    flexSensorServiceFound = true;
                }
                // Loops through available Characteristics.
                for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                    //Log.d(TAG, "Char uuid: " + gattCharacteristic.getUuid().toString());
                    //Log.d(TAG, "Char constant uuid: " + UUID_ARDUINO_FLEXSENSOR_MEASUREMENT.toString());
                    if (UUID_ARDUINO_FLEXSENSOR_MEASUREMENT.toString().equals(gattCharacteristic.getUuid().toString())) {
                        gattChar = gattCharacteristic;
                        flexSensorCharFound = true;
                    }
                }
                //Log.d(TAG, "Service: " + gattService +" Display Finished");
            }

            if (flexSensorServiceFound && flexSensorCharFound) {
                mBluetoothLeService.setCharacteristicNotification(
                        gattChar, true);
                arduinoConnected = true;

                displayData("Connected to Flex Sensor service");
                Log.i(TAG, "Connected to Flex Sensor service: " + gattServiceDisplay.getUuid().toString());
                Log.i(TAG, "Connected to Flex Sensor char: " + gattChar.getUuid().toString());

            } else {
                arduinoConnected = false;

                displayData("Can't find Flex Sensor service/nPlease Reconnect");
                Log.i(TAG, "Can't find Flex Sensor Service");

            }
            homeFragment.setArduinoConnectionStatus(arduinoConnected);

        }

        public void calibration(){
            calibrationOffset = cachedRawFlexSensor;
        }
        // End of Arduino setup
        // ////////////////////////////////////////////////////////////////////////////////////

        public void write(String accelerationData) {
            try {
                String data = " This content will append to the end of the file";

                File file = new File("javaio-appendfile.txt");

                //if file doesn't exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }

                //true = append file
                FileWriter fileWriter = new FileWriter(file.getName(), true);
                BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
                bufferWriter.write(accelerationData);
                bufferWriter.close();

                System.out.println("Done");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void HttpConnect() {
            new HttpConnectClass().execute();
        }

    @Override
    public boolean isBandConnected() {
        return bandConnected;
    }

    @Override
    public boolean isArduinoConnected() {
        return arduinoConnected;
    }


    public void saveLogOnClick(View view) {
            String FILENAME = "acceleration_log.csv";
            String entry = "test data woo!!";
            FileOutputStream out;
            try {
                out = openFileOutput(FILENAME, Context.MODE_APPEND);
                out.write(entry.getBytes());
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void writeMessage(View view) {
            String message = "This is my message!";
            String file_name = "hello_file";
            try {
                FileOutputStream fileOutputStream = openFileOutput(file_name, MODE_PRIVATE);
                fileOutputStream.write(message.getBytes());
                fileOutputStream.close();
                Toast.makeText(getApplicationContext(), "Message Saved", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        public void readMessage(View view) {
            try {
                String message;
                FileInputStream fileInputStream = openFileInput("hello_file");
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuffer = new StringBuilder();
                while ((message = bufferedReader.readLine()) != null) {
                    stringBuffer.append(message + "\n");
                }

                Log.d("PostureTrainer", stringBuffer.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        private void appendToFile(String str) {
            try {
                File file = getFileStreamPath("upload.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream writer = openFileOutput(file.getName(), MODE_APPEND | MODE_WORLD_READABLE);
                writer.write(str.getBytes());
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private String readStream(InputStream is) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int i = is.read();
                while (i != -1) {
                    bo.write(i);
                    i = is.read();
                }
                return bo.toString();
            } catch (IOException e) {
                return "";
            }
        }

        private class HttpConnectClass extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... params) {

                String FILENAME = "myFile.csv";
               /* String entry = "Ahsen" + "," + "dinc" + "\n";
                try {
                    FileOutputStream out = openFileOutput(FILENAME, Context.MODE_APPEND);

                    out.write(entry.getBytes());
                    // Log.d("Ahsen Text ", entry.toString());
                    out.close();
                    //Toast.makeText(getApplicationContext(), "Message Saved", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                try {
                    //String data = " This content will append to the end of the file";

                    File file = getFileStreamPath(FILENAME);


                    //Log.d("My Response ", Integer.toString(uploadFile(file.getAbsolutePath())));
                    String response_message = uploadFile(file.getAbsolutePath());
                    Log.d(response_message,"message");
                    if (response_message.equals("[false]")){

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Context context = getApplicationContext();
                                CharSequence toastText = "Bad Rep!!";
                                //CharSequence toastText = resultList.get(repCounter%8);
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, toastText, duration);
                                toast.show();
                         if (bandClient != null){
                            try {
                                if (vibrateON) {
                                    bandClient.getNotificationManager().vibrate(VibrationType.NOTIFICATION_TWO_TONE);
                                    ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(1000);
                                }
                                if (soundAlert)
                                    alertPlayer.start();
                            }
                            catch (BandIOException e) {
                                Log.d("ProjectX - ERROR", "Could not send vibration to band");
                            }
                        }
                            }
                        });

                        //bandClient.getNotificationManager().vibrate(VibrationType.NOTIFICATION_TWO_TONE);
                        //((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(1000);
                    }
                    if (response_message.equals("[true]")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Context context = getApplicationContext();
                                CharSequence toastText = "Good Rep!!";
                                //CharSequence toastText = resultList.get(repCounter);
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, toastText, duration);
                                toast.show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                try {
//                    JSONObject jsonObject = getJSONObjectFromURL("https://fast-river-52122.herokuapp.com/users/1/");
//
//                    // Parse your json here
//                    Log.d("ahsen", jsonObject.toString());
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                repCounter++;
                return null;
            }


        }

        public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

            HttpURLConnection urlConnection = null;

            URL url = new URL(urlString);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);

            urlConnection.setDoOutput(true);

            urlConnection.connect();

            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            char[] buffer = new char[1024];

            String jsonString = new String();

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();

            jsonString = sb.toString();

            System.out.println("JSON: " + jsonString);

            return new JSONObject(jsonString);
        }

        public static String postDataToURL(String urlString, byte[] postData) throws IOException {

            //byte[] postData       = entry.getBytes();
            int postDataLength = postData.length;
            //String request        = "https://fast-river-52122.herokuapp.com/documents/";
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            //conn.setRequestProperty("docfile", postData.toString());
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            conn.getOutputStream().write(postData);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);
            String response = sb.toString();
            return response;
        }

        public String uploadFile(String selectedFilePath) {

            int serverResponseCode = 0;
            String serverResponseMessage = "Not got";
            StringBuffer sb = new StringBuffer();
            HttpURLConnection connection;
            DataOutputStream dataOutputStream;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";


            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File selectedFile = new File(selectedFilePath);


            String[] parts = selectedFilePath.split("/");
            String fileName = parts[parts.length - 1];

            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL("https://fast-river-52122.herokuapp.com/postureapp/getrecommendation/");
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("docfile", selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"docfile\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                InputStream is = null;
                    is = connection.getInputStream();
                    int ch;

                    while ((ch = is.read()) != -1) {
                        sb.append((char) ch);
                    }
                    sb.toString();
                is.close();

                serverResponseCode = connection.getResponseCode();
                serverResponseMessage = connection.getResponseMessage();

                Log.d("Server Response is: ", serverResponseMessage + ": " + serverResponseCode);

                //response code of 201 indicates the server status OK
                if (serverResponseCode == 201) {
                    Log.d("Server Response is: ", "Uploaded");
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            return sb.toString();
        }
    }


