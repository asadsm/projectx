package com.posture.projectx.posturetrainer.ui.interfaces;

/**
 * Created by Asad on 10/31/2016.
 */

public interface IHomeFragment {

    void connect();
    void HttpConnect();
    boolean isBandConnected();
    boolean isArduinoConnected();

}
