package com.posture.projectx.posturetrainer.ui.interfaces;

import com.github.mikephil.charting.charts.LineChart;

/**
 * Created by Asad on 10/31/2016.
 */

public interface IStatsFragment {

    void addEntryToGraph(LineChart mChart);

}
