package com.posture.projectx.posturetrainer.ui.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.posture.projectx.posturetrainer.R;
import com.posture.projectx.posturetrainer.ui.interfaces.IStatsFragment;
import com.posture.projectx.posturetrainer.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IStatsFragment} interface
 * to handle interaction events.
 * Use the {@link StatsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StatsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int xCounter;
    private ViewGroup  mRootView;

    LineChart mChart;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private IStatsFragment mListener;

    public StatsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StatsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StatsFragment newInstance(String param1, String param2) {
        StatsFragment fragment = new StatsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xCounter = 0;

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView  = (ViewGroup) inflater.inflate(R.layout.fragment_stats, container, false);

        mChart = (LineChart) mRootView.findViewById(R.id.chart);



        Legend l = mChart.getLegend();

        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.BLACK);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);

        YAxis yl = mChart.getAxisLeft();
        yl.setTextColor(Color.BLACK);
        yl.setAxisMaximum(10);
        yl.setAxisMinimum(-10);

        YAxis yl2 = mChart.getAxisRight();
        yl2.setEnabled(false);


        LineData data = new LineData();
        data.setValueTextColor(Color.BLACK);
        mChart.setData(data);

        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IStatsFragment) {
            mListener = (IStatsFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void addDataToGraph(ArrayList<Constants.Labels> labels, ArrayList<Float> values){
        if (mChart == null)
            return;

        LineData data = mChart.getData();

        if (data != null) {

            List<ILineDataSet> set = data.getDataSets();

            if (set == null || set.size() == 0) {
                for (int i = 0; i < labels.size(); i++) {
                    data.addDataSet(createSet(labels.get(i)));

                }
            }


            for (int i = 0; i < labels.size(); i++){
                Constants.Labels label = labels.get(i);
                Float value            = values.get(i);
                if (label == Constants.Labels.X_ACCELERATION || label == Constants.Labels.Y_ACCELERATION ||
                        label == Constants.Labels.Z_ACCELERATION || label == Constants.Labels.MAGNITUDE)
                    value *= 5;
                data.addEntry(new Entry(xCounter, value), i);
            }

            xCounter++;
            mChart.notifyDataSetChanged();
            mChart.setVisibleXRangeMaximum(50);
            mChart.moveViewToX(data.getXMax() - 24);
        }


    }

    // Each line has it's own LineDataSet e.g for altimeter, barometer etc
    private LineDataSet createSet(Constants.Labels label){
        LineDataSet set = new LineDataSet(null, label.toString());
        set.setCubicIntensity(0.2f);
        set.setLineWidth(2f);
        set.setDrawCircles(false);
        set.setColor(Constants.colorMapping.get(label));
        return set;
    }

}
